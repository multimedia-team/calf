calf (0.90.3-4) unstable; urgency=medium

  * Bump Standards Version to 4.6.1
  * Remove obsolete d/source/local-options
  * Add libjack-jackd2-dev as optional B-D
  * Update d/source/lintian-overrides
  * Change my email address
  * Apply wrap-and-sort -ast
  * d/copyright: Add myself to the d/ section

 -- Dennis Braun <snd@debian.org>  Wed, 07 Dec 2022 10:29:04 +0100

calf (0.90.3-3) unstable; urgency=medium

  * Remove old patch 020151018~851af01 alredy present in current upstream
    release.
  * Add patch related to pitch bend working only in MIDI channel 1
    (Closes: #970631)

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Fri, 11 Mar 2022 09:35:56 -0500

calf (0.90.3-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dependency on dh-autoreconf.

  [ Dennis Braun ]
  * Bump dh-compat to 13
  * Bump Standards-Version to 4.6.0
  * Set Rules-Requires-Root: no
  * Remove all .a and .la files after installation (Closes: #1000507)
  * Update d/source/lintian-overrides
  * Mark autopkgtest as superficial (Closes: #971454)
  * Add myself as uploader
  * Remove unnecessary automake and autoconf from B-Ds

  [ Olivier Humbert ]
  * update d/control:description since ladish is out of Debian now

 -- Boyuan Yang <byang@debian.org>  Wed, 02 Feb 2022 19:55:01 -0500

calf (0.90.3-1) unstable; urgency=medium

  [ Ross Gammon ]
  * New upstream release
    - Fixes autopkgtest failure (LP: #1833840)

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0

 -- Ross Gammon <rossgammon@debian.org>  Sun, 04 Aug 2019 16:20:58 +0200

calf (0.90.2-1) experimental; urgency=medium

  * New upstream release
  * Drop 1002-lv2core_to_lv2.patch, fixed upstream
  * Add new copyright
  * Bump standards version, no changes required
  * Use latest Salsa Pipeline runner setup

 -- Ross Gammon <rossgammon@debian.org>  Sun, 09 Jun 2019 10:55:39 +0200

calf (0.90.1-2) unstable; urgency=medium

  * Fix autopkgtest failure with X virtual framebuffer (xvfb)

 -- Ross Gammon <rossgammon@debian.org>  Sat, 24 Nov 2018 18:07:58 +0100

calf (0.90.1-1) unstable; urgency=medium

  [ Jaromír Mikeš ]
  * Add myself as uploader.
  * Set priority optional.
  * Introduce postclone.sh script to ignore .pc/ dir.
  * Patches refreshed/removed.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Fixed Vcs-* stanzas to salsa.d.o
  * Updated maintainer email address
  * Bumped standards version to 4.1.3

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Erich Eickmeyer ]
  * debian/rules: clean up and modernize rules file
  * debian/control: conflict against calf-ladspa
  * Begin switch to debhelper from cdbs
  * Set debhelper compat level to 11

  [ Ross Gammon ]
  * New upstream release (Closes: #829737, #901705)
    (LP: #1769785)
  * Refresh patches
  * Unapply patches from source
  * Finish switch to debhelper
  * Fix short package description
  * Conflict against calf-ladspa, which is deprecated upstream &
    causes crashes in Ardour when both are installed (LP: #1787414)
  * Add Erich to copyright file
  * Bump version of debhelper build dependency
  * Drop useless dh-autoreconf and autotools-dev build dependencies
  * Add myself to Uploaders
  * Update copyrights & add myself
  * Drop unused & update lintian overrides
  * Drop Jonas from uploaders
  * Bump Standards Version to 4.2.1, no changes required
  * Start with simple dh debian/rules
  * Drop lintian override for missing js source, no longer in tarball
  * Enable all hardening flags
  * Drop cdbs depends, now we have switched to dh
  * Reinstate building experimental and sse flags, sse for both i386 and amd64
  * Reinstate clearout of la files
  * Close bugs in changelog
  * Reduce line length in changelog
  * Add patch to add keywords to desktop file
  * Add a simple autopkgtest
  * Add an upstream metadata file
  * Add a Gitlab CI file

 -- Ross Gammon <rossgammon@debian.org>  Thu, 15 Nov 2018 08:55:40 +0100

calf (0.0.60-5) unstable; urgency=medium

  * Update watch file: Drop uversionmangle upsetting uscan.
  * Add patches cherry-picked upstream to fix GCC 6 compilation
    (replacing patch 3001).
  * Add patches cherry-picked upstream to fix various bugs.
  * Modernize Vcs-* fields: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.0.0.
  * Update copyright info:
    + Use license shortname public-domain (not PD).
    + Extend coverage of packaging.
  * Modernize cdbs: Do copyright-check in maintainer script (not during
    build).
  * Update package relations:
    + Relax to build-depend unversioned on cdbs: Needed version
      satisfied even in oldstable.
    + Stop build-depend on devscripts libregexp-assemble-perl
      libimage-exiftool-perl libfont-ttf-perl.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 30 Jul 2017 22:55:48 -0400

calf (0.0.60-4) unstable; urgency=medium

  [ Sebastian Ramacher ]
  * Team upload.

  [ Johannes Brandstätter ]
  * Fix compilation errors when using GCC 6.

 -- Johannes Brandstätter <jbrandst@2ds.eu>  Sat, 24 Sep 2016 14:31:59 +0200

calf (0.0.60-3) unstable; urgency=medium

  * Update watch file:
    + Modernize to use format 4.
    + Mention gbp in usage hint comment.
    + Mangle upstrem prerelease versions.
  * Add patch cherry-picked upstream to fix comboboxes in Ardour4.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize copyright-check CDBS hints.
    + Strop include license-check script (part of cdbs now).
    + Tighten build-dependency on debhelper.
    + Build-depend on libipc-system-simple-perl.
    + Build-depend on libfont-ttf-perl (not lcdf-typetools).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Jun 2016 11:50:24 +0200

calf (0.0.60-2) unstable; urgency=medium

  * Extract metadata from images before copyright check.
    Build-depend on libimage-exiftool-perl libregexp-assemble-perl
    libipc-system-simple-perl lcdf-typetools.
  * Update Vcs-Git and Vcs-Browser URLs.
  * Update copyright info:
    + Fix extend coverage for main upstream authors
    + Fix bump packaging license to GPL-3+.
    + Fix update Files sections for autotools scripts and LV2 headers.
    + Fix add Files sections for jQuery-related code, and License
      sections ISC Expat.
    + Merge same-license Files sections.
    + Extend coverage of packaging.
    + Use License short-names FSFUL (and derivations) and X11.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Unfuzz patch 1002, and tidy its DEP-3 header.
  * Revive documentation (gutted in 0.0.60-1):
    + Add patch 1001 to resolve paths from autoconf.
    + Add patch 2001 to use jQuery.colorbox (not jQuery.prettyPhoto).
    + Adjust location of documentation.
    + Symlink separately packaged Javascript.
    + (Build-)depend on libjs-jquery libjs-jquery-colorbox.
    + Stop strip documentation (now DFSG compliant).
    + Put aside and regenerate autotools during build.
    + Stop include autoreconf.mk.
    + Stop build-depend on dh-autoreconf.
    + Tighten build-dependency on cdbs: Needed for routines to put
      upstream autotools files aside during build.
  * Update Homepage.
  * Update source URLs.
  * Declare compliance with Debian Policy 3.9.7.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Fix rename TODO file got get included in binary package, and drop
    done items.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Override lintian for jquery.prettyPhoto.js missing source: False
    positive.
  * Enable SSE flags for AMD64.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Mar 2016 11:23:34 +0100

calf (0.0.60-1) unstable; urgency=low

  [ Aurelien Martin ]
  * Imported Upstream version 0.0.60 (Closes: #783413)

  [ Adrian Knoth ]
  * Drop /usr/share/doc/calf/ to work around lintian errors

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Thu, 07 May 2015 14:42:11 +0200

calf (0.0.19+git20140915+5de5da28-1) unstable; urgency=medium

  * Imported Upstream version 0.0.19+git20140915+5de5da28 (Closes: #759833)

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Mon, 15 Sep 2014 22:19:42 +0200

calf (0.0.19+git20140527+e79ddef54-1) unstable; urgency=medium

  [ Adrian Knoth ]
  * Imported Upstream version 0.0.19+git20140527+e79ddef54
  * Bump standards version
  * Fix typo in package description (Closes: #745285)
  * Regenerate control

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Tue, 27 May 2014 20:48:47 +0000

calf (0.0.19+git20131202-1) unstable; urgency=low

  * Imported Upstream version 0.0.19+git20131202

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Mon, 02 Dec 2013 22:23:51 +0100

calf (0.0.19-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Update README.source to emphasize control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Add git URL as alternate source.
  * Build-depend on dh-buildinfo: Backports- and multiarch-friendly
    nowadays.
  * Do copyright-check by default: Backports-friendly nowadays.
    Build-depend on devscripts.
  * Simplify moving aside upstream cruft.
    Tighten build-dependency on cdbs.
    Fix use pseudo-comment sections in copyright file to obey silly
    restrictions of copyright format 1.0.
  * Bump packaging license to GPL-3+, and extend coverage to include
    current year.
  * Have git-buildpackage ignore upstream .gitignore files.
  * Enable build of experimental plugins.
  * Rewrite short and long descriptions, based on About page of recently
    added documentation.
  * Suggest ladish, and add hint in long description on when it is
    useful.
  * Fix build-depend explicitly on libcairo2-dev, and fix recommend
    gtk2-engines-pixbuf: Both needed since 0.0.19.
  * Fix stop build-depend on ladspa-sdk or dssi-dev: Unused since
    0.0.19.
  * Drop obsolete and unused patches.
  * Simplify patch 1002 to not include autogenerated file.

  [ Adrian Knoth ]
  * Bump standards version
  * Drop obsolete DMUA flag
  * Put aside upstream-shipped configure
  * Generate configure during build process
  * Build-depend on libtool (Closes: #702613)

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Thu, 08 Aug 2013 18:08:47 +0200

calf (0.0.19-1) experimental; urgency=low

  * New upstream release.
  * Depends on libfftw3-dev and libfluidsynth-dev.
  * Remove patches which doesn't seem to be useful anymore.
  * Rewrite patch to use lv2-dev rather than dummy lv2core.
  * Empty the dependency_libs field in .la file.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 19 Nov 2012 18:50:19 -0500

calf (0.0.18.6-5) unstable; urgency=low

  * Team upload.
  * Migrate from lv2core to lv2-dev.

 -- Alessio Treglia <alessio@debian.org>  Wed, 16 May 2012 22:17:38 +0200

calf (0.0.18.6-4) unstable; urgency=low

  [ Alessio Treglia ]
  * Add patch 0001 to migrate from libglade2 to gtkBuilder.
    Stop build-depending on libglade2-dev.

  [ Jonas Smedegaard ]
  * Bump debhelper compat level to 7.
  * Bump standards-version to 3.9.3.
  * Git-ignore .pc quilt subdir.
  * Use linux-any wildcard for libasound dependency.
    Stop build-depending on devscripts.
  * Relax to build-depend unversioned on libgtk2.0-dev, debhelper and
    libjack-dev: Required versions satisfied even in oldstable.
  * Extend my copyright for Debian packaging.
  * Rewrite copyright file using format 1.0.
  * Add patch 1001 to fix FTBFS with gcc 4.7 by including <unistd.h>.
    Closes: bug#667126. Thanks to Cyril Brulebois.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Apr 2012 12:04:07 +0200

calf (0.0.18.6-3) unstable; urgency=low

  * Team upload.

  [ Jonas Smedegaard ]
  * Adjust copyright file:
    + Extend some years.
    + Fix mention licensing exception in Files stanzas.
    + Fix use Expat as licensing short-name.
  * Suppress optional CDBS-resolved build-dependencies.

  [ Alessio Treglia ]
  * Drop LASH support.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Fri, 20 May 2011 08:49:06 +0200

calf (0.0.18.6-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Tidy copyright file: Adjust a few stanzas and declare it formatted
    according to draft DEP5 rev135.
  * Fix add a missed owner to copyright file (no new licenses).
  * Drop local CDBS snippets and locally declared DEB_MAINTAINER_MODE:
    All part of main CDBS now.
  * Refer to FSF website (not postal address) in rules file header.
  * Fix ALSA build-dependency auto-resolving (CDBS no longer horribly
    executes control.in file as a shell script).
  * Improve watch file: Add usage comment; relax pattern to non-digit
    versions and non-gzip tarball compression.
  * Fix DEP5 synatx of copyright file: append ./ in all files sections.
  * Improve copyright file: Refer to FSF website (not postal address),
    and put reference below Debian-specific comment to hint that it is
    not verbatim copied.

  [ Alessio Treglia ]
  * debian/control: Add Provides: lv2-plugin field.

  [ Adrian Knoth ]
  * Imported Upstream version 0.0.18.6
  * Bump standards version

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Tue, 24 Aug 2010 17:28:19 +0200

calf (0.0.18.5-1) unstable; urgency=low

  * Initial release (Closes: #522151)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 11 Feb 2010 21:27:47 +0100
